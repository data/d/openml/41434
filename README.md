# OpenML dataset: Click_prediction_small

https://www.openml.org/d/41434

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This is the same data as version 5 (OpenML ID = 1220) with '_id' features coded as nominal factor variables.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41434) of an [OpenML dataset](https://www.openml.org/d/41434). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41434/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41434/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41434/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

